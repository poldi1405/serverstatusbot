import discord

import configparser,os

def IsOnline(host):
	response = os.system("ping -c 1 " + host)

	# and then check the response...
	if response == 0:
		return True
	else:
		return False


#D Permissions: 256000
def run():
	Config = configparser.ConfigParser()
	try:
		Config.read('config.ini')
	except:
		print("Unable to read config.ini")
		return

	StatusText = ""
	ServerList =""
	List = ""
	if Config.get('Discord','Enabled'):
		DISCORD = discord.Client()

		@DISCORD.event
		async def on_message(message):
			# we do not want the bot to reply to itself
			if message.author == DISCORD.user:
				return

			print("Received: "+message.content)
			if message.content =='!ping':
				await DISCORD.send_message(message.channel, 'Pong!')

			if message.content =='!list':
				await DISCORD.send_message(message.channel, 'Monitored Server:\n'+StatusText)

			if message.content =='!status':
				await DISCORD.send_message(message.channel, 'Monitored Server:\n'+StatusText)

		DISCORD.run(Config.get('Discord','Token'))

	Monitor = configparser.ConfigParser().read('monitor.ini')
	while(True):
		arr = Monitor.options('Server')
		s = "Current Status:\n"
		for ser in arr:
			if IsOnline(Monitor.get('Server', ser)):
				s += Config.get('Icons', 'Online') +" "+ser+"\n"
			else:
				s += Config.get('Icons', 'Offline') +" "+ser+"\n"
		StatusText = s


if __name__ == '__main__':
	run()